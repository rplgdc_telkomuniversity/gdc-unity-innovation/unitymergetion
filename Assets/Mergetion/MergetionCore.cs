﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;

namespace Mergetion
{
    public enum MergeChoice
    {
        USE_A,
        USE_B,
        DO_NOTHING
    }

    public enum ConflictType
    {
        PROPERTIES,
        COMPONENT,
        OBJECT
    }

    [System.Serializable]
    public class ConflictReport
    {
        [Header("Description [Dont Change]")]
        [DisplayWithoutEdit()]
        public string title;
        [DisplayWithoutEdit()]
        public ConflictType type;
        [TextArea]
        public string description;

        [DisplayWithoutEdit()]
        public string propertyName;

        public CompareReference a;
        public CompareReference b;


        [Header("Decision")]
        public MergeChoice choice = MergeChoice.USE_A;
        public bool doMerge = false;

        public ConflictReport(string title, ConflictType type, string description, string propertyName, CompareReference a, CompareReference b)
        {
            this.title = title;
            this.type = type;
            this.description = description;

            this.a = a;
            this.b = b;

            this.propertyName = propertyName;

        }
    }

    public class Pair<T>
    {
        public T a;
        public T b;
        public Pair(T a, T b)
        {
            this.a = a;
            this.b = b;
        }

        public T GetExist()
        {
            return a == null ? b : a;
        }
    }

    [Serializable]
    public class CompareReference
    {
        public Transform parent;
        public GameObject gameObject;
        public Component component;
        public string displayVal;
        public object value;

        public CompareReference(GameObject obj, Component c, object val)
        {
            parent = (obj ? obj.transform.parent : null);
            gameObject = obj;
            component = c;
            value = val;
            displayVal = val.ToString();
        }

        public CompareReference(Transform parent, GameObject obj, Component c, object val)
        {
            this.parent = parent;
            gameObject = obj;
            component = c;
            value = val;
        }

    }




    public class MergetionCore
    {
        #region ReportTemplate
        const string EMPTY = "-";
        const string NULL = "NULL";
        const string EXIST = "EXIST";



        const string NULLPAIR = "|PAIR OBJ";
        const string NULLPAIR_DESC = "Can't find pair of this object\nChoose to delete or add it";

        const string CONCOMP = "|COMPONENT";
        const string CONCOMP_DESC = "Can't find pair of this component\nChoose to delete or add it";

        const string CONPROP = "|PROPERTIES";
        const string CONPROP_DESC = "Object properties is different";
        #endregion



        //LATER

        #region later

        public List<GameObject> GetAllChild(Transform parent)
        {
            List<GameObject> _result = new List<GameObject>();
            int count = parent.childCount;
            for (int i = 0; i < count; i++)
            {
                _result.Add(parent.GetChild(i).gameObject);
            }
            return _result;
        }

        public void CheckChild(GameObject a, GameObject b)
        {

            List<GameObject> aObj = GetAllChild(a.transform);
            List<GameObject> bObj = GetAllChild(b.transform);

            bool exist = false;
            for (int i = aObj.Count; i > 0; i--)
            {
                exist = false;
                for (int j = bObj.Count; j > 0; j--)
                {
                    if (aObj[i].name == bObj[j].name)
                    {

                    }
                }
            }
        }
        #endregion
        #region Utils
        public void CombineList<T>(List<T> from, List<T> to)
        {
            from.ForEach(val => to.Add(val));
            from.Clear();
        }
        #endregion

        #region Conflict_Solving

        public void SolveConflict(ConflictReport report)
        {
            if (!report.doMerge || report.choice == MergeChoice.DO_NOTHING)
                return;

            switch (report.type)
            {
                case ConflictType.PROPERTIES:
                    SolveConflictProperties(report);
                    break;
                case ConflictType.COMPONENT:
                    SolveConflictComponent(report);
                    break;
                case ConflictType.OBJECT:
                    SolveConflictObject(report);
                    break;
            }
        }

        void SolveConflictProperties(ConflictReport report)
        {
            Component from = report.choice == MergeChoice.USE_A ? report.a.component : report.b.component;
            Component target = report.choice == MergeChoice.USE_B ? report.a.component : report.b.component;

            Type fromType = from.GetType();
            PropertyInfo fromPropInfo = fromType.GetProperty(report.propertyName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

            Type targetType = target.GetType();
            PropertyInfo targetPropInfo = targetType.GetProperty(report.propertyName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

            targetPropInfo.SetValue(target, fromPropInfo.GetValue(from));
        }

        void SolveConflictComponent(ConflictReport report)
        {
            Component from = report.choice == MergeChoice.USE_A ? report.a.component : report.b.component;
            Component target = report.choice == MergeChoice.USE_B ? report.a.component : report.b.component;

            if (from == null)
            {
                //destroy target
                MonoBehaviour.DestroyImmediate(target);
            }
            else
            {
                //add component to target
                if (report.choice == MergeChoice.USE_A)
                    report.b.gameObject.AddComponent(from.GetType());
                else
                    report.a.gameObject.AddComponent(from.GetType());
            }
        }

        void SolveConflictObject(ConflictReport report)
        {
            GameObject from = report.choice == MergeChoice.USE_A ? report.a.gameObject : report.b.gameObject;
            GameObject target = report.choice == MergeChoice.USE_B ? report.a.gameObject : report.b.gameObject;

            if (from == null)
            {
                //destroy target
                MonoBehaviour.DestroyImmediate(target);
            }
            else
            {
                //add gameObject to target
                if (report.choice == MergeChoice.USE_A)
                {
                    GameObject temp = MonoBehaviour.Instantiate(from, report.b.parent);
                    temp.name = from.name;
                }
                else
                {
                    GameObject temp = MonoBehaviour.Instantiate(from, report.a.parent);
                    temp.name = from.name;
                }
            }
        }

        #endregion

        #region ObjectComparer
        public List<GameObject> GetAllChild(GameObject target)
        {
            List<GameObject> childs = new List<GameObject>();

            foreach (Transform childT in target.transform)
            {
                childs.Add(childT.gameObject);
            }
            return childs;
        }

        /// <summary>
        /// Compare name of each child of an object.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="pairMatch"></param>
        /// <param name="pairMiss"></param>
        /// <returns>return isConflict?</returns>
        public bool CompareChild(GameObject a, GameObject b, List<Pair<GameObject>> pairMatch, List<Pair<GameObject>> pairMiss)
        {
            List<GameObject> childA = GetAllChild(a);
            List<GameObject> childB = GetAllChild(b);

            int lenA = childA.Count;
            for (int i = lenA - 1; i >= 0; i--)
            {
                Pair<GameObject> pair = new Pair<GameObject>(childA[i], null);
                int lenB = childB.Count;
                for (int j = lenB - 1; j >= 0; j--)
                {
                    Debug.Log(i + " : " + j);
                    if (childA[i].name == childB[j].name)
                    {
                        pair.b = childB[j];
                        childB.RemoveAt(j);
                    }
                }
                if (pair.b == null)
                {
                    pairMiss.Add(pair);
                }
                else
                {
                    pairMatch.Add(pair);
                }
            }

            foreach (GameObject cB in childB)
            {
                pairMiss.Add(new Pair<GameObject>(null, cB));
            }

            return pairMiss.Count != 0;
        }

        /// <summary>
        /// Compare is pair 1 object
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name=""></param>
        /// <returns>return isConflict?</returns>
        public bool CompareObject(GameObject a, GameObject b, GameObject parentA, GameObject parentB, List<ConflictReport> report)
        {
            if (a == null || b == null)
            {
                if (report != null)
                {
                    string name = (a == null ? b.name : a.name);
                    Transform pA = parentA ? parentA.transform : null;
                    Transform pB = parentB ? parentB.transform : null;

                    CompareReference cA = new CompareReference(pA, a, null, (a == null ? NULL : EXIST));
                    CompareReference cB = new CompareReference(pB, b, null, (b == null ? NULL : EXIST));
                    ConflictReport conflict = new ConflictReport(name + NULLPAIR, ConflictType.OBJECT, NULLPAIR_DESC,
                                                                EMPTY, cA, cB);
                    report.Add(conflict);
                }
                return true;
            }
            else if (a.name != b.name)
            {
                if (report != null)
                {
                    Transform pA = parentA ? parentA.transform : null;
                    Transform pB = parentB ? parentB.transform : null;
                    CompareReference cA = new CompareReference(pA, a, a.transform, a.name);
                    CompareReference cB = new CompareReference(pB, b, b.transform, b.name);
                    ConflictReport conflict = new ConflictReport(a.name + NULLPAIR, ConflictType.OBJECT, NULLPAIR_DESC,
                                                                EMPTY, cA, cB);
                    report.Add(conflict);
                }
                return true;

            }
            return false;
        }
        #endregion

        #region ComponentComparer
        public List<Component> GetAllComponent(GameObject target)
        {
            Component[] cs = target.GetComponents(typeof(Component));
            return new List<Component>(cs);
        }

        /// <summary>
        /// Compare name of each component of an object.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="pairMatch"></param>
        /// <param name="pairMiss"></param>
        /// <returns>return isConflict?</returns>
        public bool CompareComponent(GameObject a, GameObject b, List<Pair<Component>> pairMatch, List<Pair<Component>> pairMiss)
        {
            List<Component> compA = GetAllComponent(a);
            List<Component> compB = GetAllComponent(b);

            int lenA = compA.Count;
            for (int i = lenA - 1; i >= 0; i--)
            {
                Pair<Component> pair = new Pair<Component>(compA[i], null);
                int lenB = compB.Count;
                for (int j = lenB - 1; j >= 0; j--)
                {
                    if (compA[i].GetType() == compB[j].GetType())
                    {
                        pair.b = compB[j];
                        compB.RemoveAt(j);
                    }
                }
                if (pair.b == null)
                {
                    pairMiss.Add(pair);
                }
                else
                {
                    pairMatch.Add(pair);
                }
            }

            foreach (Component cB in compB)
            {
                pairMiss.Add(new Pair<Component>(null, cB));
            }

            return pairMiss.Count != 0;
        }
        #endregion

        #region PropertiesComparer
        public bool IsList(object o)
        {
            if (o == null) return false;
            return o is IList &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        public bool IsPrimitiveType(Type type)
        {
            return type.IsPrimitive ||
                type.IsEnum ||
                type == typeof(string) ||
                type == typeof(decimal) ||
                type == typeof(DateTime) ||
                type == typeof(DateTimeOffset) ||
                type == typeof(TimeSpan) ||
                type == typeof(Guid);
        }

        public bool IsNull(object T)
        {
            return T.ToString() == "null" ? true : false;
        }

        private bool CompareEnumerations(object value1, object value2)
        {
            // if one of the values is null, no need to proceed return false;
            if (value1 == null && value2 != null || value1 != null && value2 == null)
                return false;
            else if (value1 != null && value2 != null)
            {

                var enumerator1 = ((IEnumerable)value1).GetEnumerator();
                var enumerator2 = ((IEnumerable)value2).GetEnumerator();

                bool nextAvailable1 = true;
                bool nextAvailable2 = true;
                bool isMatch = true;
                while (nextAvailable1 && nextAvailable2 && isMatch)
                {
                    nextAvailable1 = enumerator1.MoveNext();
                    nextAvailable2 = enumerator2.MoveNext();
                    if (nextAvailable1 && nextAvailable2)
                    {
                        object enumValue1Item = enumerator1.Current;
                        object enumValue2Item = enumerator2.Current;

                        bool null1 = IsNull(enumValue1Item);
                        bool null2 = IsNull(enumValue2Item);
                        if (null1 || null2)
                        {
                            if (null1 != null2)
                            {
                                isMatch = false;
                                //Debug.Log("OneValueNull");
                            }

                            continue;
                        }
                        Type enumValue1ItemType = enumValue1Item.GetType();

                        if (IsPrimitiveType(enumValue1ItemType))
                        {
                            if (enumValue1Item.ToString() != enumValue2Item.ToString())
                            {
                                isMatch = false;
                                //Debug.Log("ValueConflict");
                            }
                        }
                        else
                        {

                            if (enumValue1Item is Component)
                            {
                                isMatch = !CompareObject(((Component)enumValue1Item).gameObject, ((Component)enumValue2Item).gameObject, null, null, null);
                                //Debug.Log("GameObject M");
                                //Debug.Log(isMatch);
                            }
                            else if (!enumValue1Item.Equals(enumValue2Item))
                            {
                                //Debug.Log("Reff Conflict");
                                isMatch = false;
                            }
                        }
                    }
                }

                if ((nextAvailable1 == nextAvailable2) && isMatch)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            return true;
        }



        public List<FieldInfo> GetAllProperties(object target)
        {
            List<FieldInfo> result = new List<FieldInfo>();

            Type typeA = target.GetType();
            foreach (FieldInfo field in typeA.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                result.Add(field);
            }

            return result;
        }


        public bool CompareCustomObject(object a, object b, int limit = 0)
        {
            FieldInfo[] fA = GetAllProperties(a).ToArray();
            FieldInfo[] fB = GetAllProperties(b).ToArray();

            int lenA = fA.Length;

            for (int i = 0; i < lenA; i++)
            {
                object objA = fA[i].GetValue((object)a);
                object objB = fB[i].GetValue((object)b);

                Type t;
                if (objA == null || objB == null)
                {
                    if (objA == objB)
                        continue;
                    else
                    {
                        object temp = objA == null ? objB : objA;
                        t = temp.GetType();
                    }
                }
                else
                {
                    t = objA.GetType();
                }

                if (IsPrimitiveType(t))
                {
                    //Debug.Log("Primitive");
                    // If Primitive, compare Value
                    if (objA.ToString() != objB.ToString())
                    {
                        return false;
                    }
                }
                else
                {

                    //Debug.Log("Non-Primitive");
                    //if not primitive compare via reference
                    if (objA != null && objB != null && objA.GetType().IsSerializable)
                    {
                        if (objA is Array || IsList(objA))
                        {
                            //Debug.Log("ARRAY");
                            if (!CompareEnumerations(objA, objB))
                            {
                                return false;
                            }
                        }
                        else
                        {
                            if (limit < 2)
                                return CompareCustomObject(objA, objB, limit + 1);
                        }
                    }
                    else if (!objA.Equals(objB))
                    {
                        return false;
                    }


                }
            }
            return true;
        }

        /// <summary>
        /// Compare name of each component of an object.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="pairMatch"></param>
        /// <param name="pairMiss"></param>
        /// <returns>return isConflict?</returns>
        public bool CompareProperties(object a, object b, List<Pair<FieldInfo>> pairMiss)
        {
            FieldInfo[] fA = GetAllProperties(a).ToArray();
            FieldInfo[] fB = GetAllProperties(b).ToArray();

            int lenA = fA.Length;

            for (int i = 0; i < lenA; i++)
            {
                //Debug.Log(fA[i].Name + " : " + fB[i].Name + "  |  " + fA[i].GetValue((object)a) + " : " + fB[i].GetValue((object)b));

                object objA = fA[i].GetValue((object)a);
                object objB = fB[i].GetValue((object)b);

                Type t;
                if (objA == null || objB == null)
                {
                    if (objA == objB)
                        continue;
                    else
                    {
                        object temp = objA == null ? objB : objA;
                        t = temp.GetType();
                    }
                }
                else
                {
                    t = objA.GetType();
                }

                if (IsPrimitiveType(t))
                {
                    //Debug.Log("Primitive");
                    // If Primitive, compare Value
                    if (objA.ToString() != objB.ToString())
                    {
                        pairMiss.Add(new Pair<FieldInfo>(fA[i], fB[i]));
                        //Debug.Log("MISS");
                    }
                }
                else
                {

                    //Debug.Log("Non-Primitive");
                    //if not primitive compare via reference
                    if (objA != null && objB != null && objA.GetType().IsSerializable)
                    {
                        if (objA is Array || IsList(objA))
                        {
                            if (!CompareEnumerations(objA, objB))
                            {
                                pairMiss.Add(new Pair<FieldInfo>(fA[i], fB[i]));
                            }
                        }
                        else
                        {
                            Debug.Log("CUSTOM OBJ");
                            CompareCustomObject(objA, objB);
                        }
                    }
                    else if (!objA.Equals(objB))
                    {
                        pairMiss.Add(new Pair<FieldInfo>(fA[i], fB[i]));
                    }


                }

                //Debug.Log("====================================================");
            }

            return pairMiss.Count != 0;
        }
        #endregion

        #region MAIN
        public List<ConflictReport> FindDifference(GameObject a, GameObject b, GameObject parentA, GameObject parentB)
        {
            List<ConflictReport> report = new List<ConflictReport>();

            //Compare Object
            if (CompareObject(a, b, parentA, parentB, report))
                return report;

            //Compare Parent Component
            List<Pair<Component>> pairsMatch = new List<Pair<Component>>();
            List<Pair<Component>> pairsMiss = new List<Pair<Component>>();
            CompareComponent(a, b, pairsMatch, pairsMiss);

            foreach (Pair<Component> pair in pairsMiss)
            {

                CompareReference cA = new CompareReference(a, pair.a, (pair.a == null ? NULL : EXIST));
                CompareReference cB = new CompareReference(b, pair.b, (pair.b == null ? NULL : EXIST));
                ConflictReport conflict = new ConflictReport(a.name + CONCOMP, ConflictType.COMPONENT, CONCOMP_DESC,
                                                            "Comp : " + pair.GetExist().GetType().ToString(), cA, cB);

                report.Add(conflict);
            }

            //Compare Parent Properties (execute : pairsMatch)
            List<Pair<FieldInfo>> missProperties = new List<Pair<FieldInfo>>();
            foreach (Pair<Component> pair in pairsMatch)
            {
                missProperties.Clear();
                CompareProperties(pair.a, pair.b, missProperties);

                foreach (Pair<FieldInfo> field in missProperties)
                {
                    CompareReference cA = new CompareReference(a, pair.a, field.a.GetValue((object)pair.a));
                    CompareReference cB = new CompareReference(b, pair.b, field.b.GetValue((object)pair.b));

                    ConflictReport conflict = new ConflictReport(a.name + CONPROP, ConflictType.PROPERTIES, CONPROP_DESC,
                                                                field.a.Name, cA, cB);

                    report.Add(conflict);
                }

            }





            //CompareChilds
            List<Pair<GameObject>> pairChildMatch = new List<Pair<GameObject>>();
            List<Pair<GameObject>> pairChildMiss = new List<Pair<GameObject>>();
            CompareChild(a, b, pairChildMatch, pairChildMiss);

            foreach (Pair<GameObject> pair in pairChildMiss)
            {
                CompareReference cA = new CompareReference(a.transform, pair.a, null, (pair.a == null ? NULL : EXIST));
                CompareReference cB = new CompareReference(b.transform, pair.b, null, (pair.b == null ? NULL : EXIST));

                ConflictReport conflict = new ConflictReport(pair.GetExist().name + NULLPAIR, ConflictType.OBJECT, NULLPAIR_DESC,
                                                            "Object", cA, cB);

                report.Add(conflict);
            }

            foreach (Pair<GameObject> pair in pairChildMatch)
            {
                FindDifference(pair.a, pair.b, a, b);
            }

            return report;
        }
        #endregion

    }
}

